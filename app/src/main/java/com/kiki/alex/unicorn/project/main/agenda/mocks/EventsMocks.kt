package com.kiki.alex.unicorn.project.main.agenda.mocks

import com.kiki.alex.unicorn.project.main.agenda.model.EventApi
import java.util.concurrent.atomic.AtomicInteger

object EventsMocks {

    private val counter = AtomicInteger(0)

    fun getEvents(): List<EventApi> {
        return ArrayList<EventApi>().apply {
            counter.set(0)
            for (i in 1..20) {
                add(createEvent(counter.incrementAndGet()))
            }
        }
    }

    private fun createEvent(modifier: Int): EventApi {
        return EventApi(
            id = "event-id-$modifier",
            name = "Event name $modifier",
            description = "Descriptions of the event $modifier"
        )
    }

}