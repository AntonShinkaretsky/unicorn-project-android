package com.kiki.alex.unicorn.project.main.places.list.recycler

import com.kiki.alex.unicorn.project.utils.recycler.AdapterListener
import com.kiki.alex.unicorn.project.utils.recycler.BaseListAdapter

class PlacesAdapter(listener: AdapterListener) : BaseListAdapter(
    PlaceCell,
    listener = listener
)