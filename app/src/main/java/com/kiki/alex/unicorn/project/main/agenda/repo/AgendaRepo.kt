package com.kiki.alex.unicorn.project.main.agenda.repo

import androidx.lifecycle.LiveData
import com.kiki.alex.unicorn.project.main.agenda.model.Event
import com.kiki.alex.unicorn.project.main.agenda.model.EventMapper.toUi
import com.kiki.alex.unicorn.project.main.agenda.model.EventsApi
import com.kiki.alex.unicorn.project.main.agenda.model.EventsDao
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class AgendaRepo(
    private val apiEvents: EventsApi,
    private val daoEvents: EventsDao
) {

    private var getEvents = Disposables.disposed()

    fun getEvent(id: String): LiveData<Event> {
        return daoEvents.get(id)
    }

    fun getEventsLive(): LiveData<List<Event>> {
        return daoEvents.getAllLive()
    }

    fun apiGetEvents() {
        // TODo loading state?
        getEvents.dispose()
        getEvents = apiEvents
            .getEvents()
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { eventsApi ->
                    val events = eventsApi.map { toUi(it) }
                    daoEvents.insertAll(events)
                    getEvents.dispose()
                },
                onError = {
                    getEvents.dispose()
                    // TODO
                }
            )
    }

}