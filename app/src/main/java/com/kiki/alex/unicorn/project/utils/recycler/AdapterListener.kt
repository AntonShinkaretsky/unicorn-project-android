package com.kiki.alex.unicorn.project.utils.recycler

interface AdapterListener {
    fun listen(click: AdapterClick?)
}

fun adapterListener(onClick: (click: AdapterClick?) -> Unit): AdapterListener {
    return object : AdapterListener {
        override fun listen(click: AdapterClick?) {
            onClick(click)
        }
    }
}