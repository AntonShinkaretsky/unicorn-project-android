package com.kiki.alex.unicorn.project.main.agenda.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.kiki.alex.unicorn.project.utils.recycler.AdapterClick
import com.kiki.alex.unicorn.project.utils.recycler.RecyclerItem

@Entity
data class Event(
    @PrimaryKey
    override val id: String,
    val name: String,
    val description: String
) : RecyclerItem, AdapterClick