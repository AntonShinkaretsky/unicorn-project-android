package com.kiki.alex.unicorn.project.main.places

import androidx.core.os.bundleOf
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.main.places.details.PlaceDetailsFragment.Companion.BUNDLE_KEY_PLACE_ID
import com.kiki.alex.unicorn.project.navigation.AppNavigator
import com.kiki.alex.unicorn.project.navigation.NavigatorAction

fun AppNavigator.navigateToPlaceDetails(placeId: String) {
    val action = NavigatorAction.Forward(
        R.id.fragment_container_main,
        R.id.action_placesListFragment_to_placeDetailsFragment,
        bundleOf(BUNDLE_KEY_PLACE_ID to placeId)
    )
    navigate(action)
}