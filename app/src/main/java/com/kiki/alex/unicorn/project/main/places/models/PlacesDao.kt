package com.kiki.alex.unicorn.project.main.places.models

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PlacesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(place: List<Place>)

    @Query("SELECT * FROM place WHERE id = :id")
    fun get(id: String): LiveData<Place>

    @Query("SELECT * FROM place")
    fun getAllLive(): LiveData<List<Place>>

}