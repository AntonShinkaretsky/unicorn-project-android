package com.kiki.alex.unicorn.project.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kiki.alex.unicorn.project.main.agenda.model.Event
import com.kiki.alex.unicorn.project.main.agenda.model.EventsDao
import com.kiki.alex.unicorn.project.main.places.models.Place
import com.kiki.alex.unicorn.project.main.places.models.PlacesDao

@Database(
    entities = [Event::class, Place::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun daoEvents(): EventsDao
    abstract fun daoPlaces(): PlacesDao

    companion object {
        const val DATABASE_NAME = "main-db"

        const val DB_TRUE = 1
        const val DB_FALSE = 0
    }

}