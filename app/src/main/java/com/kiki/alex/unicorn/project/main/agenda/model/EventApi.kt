package com.kiki.alex.unicorn.project.main.agenda.model

class EventApi(
    val id: String,
    val name: String,
    val description: String
)