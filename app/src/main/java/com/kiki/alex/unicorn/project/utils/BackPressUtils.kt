package com.kiki.alex.unicorn.project.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

interface BackPressedListener {
    fun usesBackPressed(): Boolean
}

// This method allows you to get child fragment of AppCompatActivity, if it is currently active,
// and is in Navigation Component fragment container
fun AppCompatActivity.getNavigationChildFragment(): Fragment? {
    return supportFragmentManager.primaryNavigationFragment
        ?.childFragmentManager?.primaryNavigationFragment
}

fun AppCompatActivity.childUsesBackPressed(): Boolean {
    val fragment = getNavigationChildFragment() as? BackPressedListener
    // Share click with nested fragment
    return fragment != null && fragment.usesBackPressed()
}

// This method allows you to get child fragment of Fragment, if it is currently active,
// and is in Navigation Component fragment container
fun Fragment.getNavigationChildFragment(): Fragment? {
    return childFragmentManager.primaryNavigationFragment
        ?.childFragmentManager?.primaryNavigationFragment
}

fun Fragment.childUsesBackPressed(): Boolean {
    val fragment = getNavigationChildFragment() as? BackPressedListener
    // Share click with nested fragment
    return fragment != null && fragment.usesBackPressed()
}