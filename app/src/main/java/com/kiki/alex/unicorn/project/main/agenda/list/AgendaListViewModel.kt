package com.kiki.alex.unicorn.project.main.agenda.list

import androidx.lifecycle.ViewModel
import com.kiki.alex.unicorn.project.main.agenda.navigateToEventDetails
import com.kiki.alex.unicorn.project.main.agenda.repo.AgendaRepo
import com.kiki.alex.unicorn.project.navigation.AppNavigator

class AgendaListViewModel(
    private val navigator: AppNavigator,
    private val repo: AgendaRepo
) : ViewModel() {

    val list = repo.getEventsLive()

    init {
        repo.apiGetEvents()
    }

    fun navigateToEventDetails(eventId: String) {
        navigator.navigateToEventDetails(eventId)
    }

}