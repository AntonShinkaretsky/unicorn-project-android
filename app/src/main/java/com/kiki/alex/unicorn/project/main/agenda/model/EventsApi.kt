package com.kiki.alex.unicorn.project.main.agenda.model

import io.reactivex.Single
import retrofit2.http.GET

interface EventsApi {

    @GET("events") // TODO update endpoint
    fun getEvents(): Single<List<EventApi>>

}