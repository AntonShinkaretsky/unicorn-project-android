package com.kiki.alex.unicorn.project.utils.recycler

interface RecyclerItem {
    val id: String?
    override fun equals(other: Any?): Boolean
}