package com.kiki.alex.unicorn.project.main.places.list

import androidx.lifecycle.ViewModel
import com.kiki.alex.unicorn.project.main.places.navigateToPlaceDetails
import com.kiki.alex.unicorn.project.main.places.repo.PlacesRepo
import com.kiki.alex.unicorn.project.navigation.AppNavigator

class PlacesListViewModel(
    private val navigation: AppNavigator,
    private val repo: PlacesRepo
) : ViewModel() {

    val list = repo.getPlacesLive()

    init {
        repo.apiGetPlaces()
    }

    fun navigateToPlaceDetails(placeId: String) {
        navigation.navigateToPlaceDetails(placeId)
    }

}