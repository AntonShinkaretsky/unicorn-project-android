package com.kiki.alex.unicorn.project.main.places.list.recycler

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.main.agenda.model.Event
import com.kiki.alex.unicorn.project.main.places.models.Place
import com.kiki.alex.unicorn.project.utils.recycler.AdapterListener
import com.kiki.alex.unicorn.project.utils.recycler.RecyclerItem
import com.partners.accountability.utils.recycler.Cell

object PlaceCell : Cell<RecyclerItem>() {

    override fun belongsTo(item: RecyclerItem?): Boolean {
        return item is Place
    }

    override fun type(): Int {
        return R.layout.item_place
    }

    override fun holder(parent: ViewGroup): RecyclerView.ViewHolder {
        return PlaceViewHolder(
            parent.viewOf(type())
        )
    }

    override fun bind(
        holder: RecyclerView.ViewHolder,
        item: RecyclerItem?,
        listener: AdapterListener?
    ) {
        if (holder is PlaceViewHolder && item is Place) {
            holder.bind(item)
            holder.itemView.setOnClickListener {
                listener?.listen(item)
            }
        }
    }

}