package com.kiki.alex.unicorn.project.mocks

var DEBOUNCE: Long = 2000
const val TEST_DEBOUNCE: Long = 0

fun getMockedDebounce(): Long {
    return DEBOUNCE
}

fun getTestDebounce(): Long {
    return TEST_DEBOUNCE
}