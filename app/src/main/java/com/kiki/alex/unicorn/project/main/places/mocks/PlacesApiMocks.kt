package com.kiki.alex.unicorn.project.main.places.mocks

import com.kiki.alex.unicorn.project.main.places.models.PlaceApi
import com.kiki.alex.unicorn.project.main.places.models.PlacesApi
import com.kiki.alex.unicorn.project.mocks.getMockedDebounce
import io.reactivex.Single
import java.util.concurrent.TimeUnit

object PlacesApiMocks : PlacesApi {

    override fun getPlaces(): Single<List<PlaceApi>> {
        return Single
            .just(PlacesMocks.getPlaces())
            .delay(getMockedDebounce(), TimeUnit.MILLISECONDS)
    }

}