package com.kiki.alex.unicorn.project.main.agenda.model

object EventMapper {

    fun toUi(event: EventApi): Event {
        return Event(
            id = event.id,
            name = event.name,
            description = event.description
        )
    }

}