package com.kiki.alex.unicorn.project

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class UnicornApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(allAppModules)
            androidContext(this@UnicornApplication)
        }
    }

}