package com.kiki.alex.unicorn.project.utils

import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.FragmentActivity
import com.kiki.alex.unicorn.project.exception.hideKeyboard

fun Toolbar.setDefaultBackBehaviour(activity: FragmentActivity) {
    setNavigationOnClickListener {
        activity.hideKeyboard()
        activity.onBackPressedDispatcher.onBackPressed()
    }
}