package com.kiki.alex.unicorn.project.main.places.mocks

import com.kiki.alex.unicorn.project.main.places.models.PlaceApi
import java.util.concurrent.atomic.AtomicInteger

object PlacesMocks {

    private val counter = AtomicInteger(0)

    fun getPlaces(): List<PlaceApi> {
        return ArrayList<PlaceApi>().apply {
            counter.set(0)
            for (i in 1..20) {
                add(createEvent(counter.incrementAndGet()))
            }
        }
    }

    private fun createEvent(modifier: Int): PlaceApi {
        return PlaceApi(
            id = "place-id-$modifier",
            name = "Place name $modifier",
            description = "Descriptions of the place $modifier",
            latitude = 52.363764 + modifier,
            longitude = 4.866482 + (modifier / 2)
        )
    }


}