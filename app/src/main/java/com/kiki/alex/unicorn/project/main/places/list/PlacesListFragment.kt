package com.kiki.alex.unicorn.project.main.places.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.main.places.list.recycler.PlacesAdapter
import com.kiki.alex.unicorn.project.main.places.models.Place
import com.kiki.alex.unicorn.project.utils.recycler.adapterListener
import kotlinx.android.synthetic.main.fragment_places_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class PlacesListFragment : Fragment(R.layout.fragment_places_list) {

    private val vm: PlacesListViewModel by viewModel()

    private val listener = adapterListener {
        when (it) {
            is Place -> vm.navigateToPlaceDetails(it.id)
        }
    }

    private val listAdapter by lazy { PlacesAdapter(listener) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        observe()
    }

    private fun initialize() {
        initializeRecycler()
    }

    private fun observe() {
        observeList()
    }

    private fun initializeRecycler() {
        recycler_places.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }

    private fun observeList() {
        vm.list.observe(viewLifecycleOwner, Observer { list ->
            listAdapter.submitList(list)
        })
    }

}