package com.kiki.alex.unicorn.project.main.agenda.list.recycler

import com.kiki.alex.unicorn.project.utils.recycler.AdapterListener
import com.kiki.alex.unicorn.project.utils.recycler.BaseListAdapter

class EventsAdapter(listener: AdapterListener) : BaseListAdapter(
    EventCell,
    listener = listener
)