package com.kiki.alex.unicorn.project.main.places.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.kiki.alex.unicorn.project.utils.recycler.AdapterClick
import com.kiki.alex.unicorn.project.utils.recycler.RecyclerItem

@Entity
data class Place(
    @PrimaryKey
    override val id: String,
    val name: String,
    val description: String,
    val latitude: Double,
    val longitude: Double
) : RecyclerItem, AdapterClick