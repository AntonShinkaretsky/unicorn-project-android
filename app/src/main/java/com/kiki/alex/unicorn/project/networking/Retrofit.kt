package com.kiki.alex.unicorn.project.networking

import com.google.gson.GsonBuilder
import com.jakewharton.rxbinding2.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class Retrofit {

    val instance: Retrofit by lazy { initRetrofit() }

    private fun initRetrofit(): Retrofit {
        val gson = GsonBuilder()
            .create()

        val httpClient = OkHttpClient.Builder()
            .addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
                    else HttpLoggingInterceptor.Level.NONE
                }
            )
            .build()

        return Retrofit.Builder()
            .baseUrl(API_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(httpClient)
            .build()
    }

    companion object {

        private const val BASE_URL = "https://kiki-alex.unicorn-project.com/"
        private const val API_URL = "${BASE_URL}api/"
    }

}