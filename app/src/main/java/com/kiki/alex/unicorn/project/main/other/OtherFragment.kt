package com.kiki.alex.unicorn.project.main.other

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.kiki.alex.unicorn.project.R

class OtherFragment : Fragment(R.layout.fragment_other)
