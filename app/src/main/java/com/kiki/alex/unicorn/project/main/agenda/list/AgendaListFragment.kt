package com.kiki.alex.unicorn.project.main.agenda.list

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.main.agenda.list.recycler.EventsAdapter
import com.kiki.alex.unicorn.project.main.agenda.model.Event
import com.kiki.alex.unicorn.project.main.places.list.recycler.PlacesAdapter
import com.kiki.alex.unicorn.project.utils.recycler.adapterListener
import kotlinx.android.synthetic.main.fragment_agenda_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class AgendaListFragment : Fragment(R.layout.fragment_agenda_list) {

    private val vm: AgendaListViewModel by viewModel()

    private val listener = adapterListener {
        when (it) {
            is Event -> vm.navigateToEventDetails(it.id)
        }
    }

    private val listAdapter by lazy { EventsAdapter(listener) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        observe()
    }

    private fun initialize() {
        initializeRecycler()
    }

    private fun observe() {
        observeList()
    }

    private fun initializeRecycler() {
        recycler_events.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }
    }

    private fun observeList() {
        vm.list.observe(viewLifecycleOwner, Observer { list ->
            listAdapter.submitList(list)
        })
    }

}