package com.kiki.alex.unicorn.project.main.agenda

import com.kiki.alex.unicorn.project.db.AppDatabase
import com.kiki.alex.unicorn.project.main.agenda.list.AgendaListViewModel
import com.kiki.alex.unicorn.project.main.agenda.mocks.EventsApiMocks
import com.kiki.alex.unicorn.project.main.agenda.model.EventsApi
import com.kiki.alex.unicorn.project.main.agenda.repo.AgendaRepo
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val agendaModule = module {
    //    single { get<Retrofit>().instance.create(EventsApi::class.java) }
    single { EventsApiMocks as EventsApi }
    single { get<AppDatabase>().daoEvents() }

    single {
        AgendaRepo(
            apiEvents = get(),
            daoEvents = get()
        )
    }

    viewModel {
        AgendaListViewModel(
            navigator = get(),
            repo = get()
        )
    }
}