package com.kiki.alex.unicorn.project.main.places.list.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.kiki.alex.unicorn.project.main.agenda.model.Event
import com.kiki.alex.unicorn.project.main.places.models.Place
import kotlinx.android.synthetic.main.item_place.view.*

class PlaceViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(place: Place) {
        itemView.text_place_location.text = "Latitude: ${place.latitude}, Longitude: ${place.longitude}"
    }

}