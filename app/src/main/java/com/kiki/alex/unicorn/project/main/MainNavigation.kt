package com.kiki.alex.unicorn.project.main

enum class MainNavigation {
    AGENDA,
    PLACES,
    OTHER_1,
    OTHER_2
}