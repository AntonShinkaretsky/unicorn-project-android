package com.kiki.alex.unicorn.project.main.agenda.mocks

import com.kiki.alex.unicorn.project.main.agenda.model.EventApi
import com.kiki.alex.unicorn.project.main.agenda.model.EventsApi
import com.kiki.alex.unicorn.project.mocks.getMockedDebounce
import io.reactivex.Single
import java.util.concurrent.TimeUnit

object EventsApiMocks : EventsApi {

    override fun getEvents(): Single<List<EventApi>> {
        return Single
            .just(EventsMocks.getEvents())
            .delay(getMockedDebounce(), TimeUnit.MILLISECONDS)
    }

}