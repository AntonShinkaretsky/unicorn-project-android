package com.kiki.alex.unicorn.project.exception

import android.app.Activity

fun Activity.hideKeyboard() {
    val view = currentFocus ?: findViewById(android.R.id.content)
    view.hideKeyboard()
}