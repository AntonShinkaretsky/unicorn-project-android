package com.kiki.alex.unicorn.project.main.places.details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.utils.setDefaultBackBehaviour
import kotlinx.android.synthetic.main.fragment_place_details.*

class PlaceDetailsFragment : Fragment(R.layout.fragment_place_details) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
        observe()
    }

    private fun initialize() {
        initializeToolbar()
    }

    private fun observe() {

    }

    private fun initializeToolbar() {
        toolbar_place_details.setDefaultBackBehaviour(requireActivity())
    }

    companion object {
        const val BUNDLE_KEY_PLACE_ID = "bundle_key_place_id"
    }

}