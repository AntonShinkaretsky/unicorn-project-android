package com.kiki.alex.unicorn.project

import androidx.room.Room
import com.kiki.alex.unicorn.project.db.AppDatabase
import com.kiki.alex.unicorn.project.db.AppDatabase.Companion.DATABASE_NAME
import com.kiki.alex.unicorn.project.main.agenda.agendaModule
import com.kiki.alex.unicorn.project.main.mainModule
import com.kiki.alex.unicorn.project.main.places.placesModule
import com.kiki.alex.unicorn.project.navigation.AppNavigator
import com.kiki.alex.unicorn.project.navigation.ViewNavigator
import org.koin.dsl.module

val navigationModule = module {
    single { AppNavigator() }
    factory { (activity: AppActivity) -> ViewNavigator(activity) }
}

val dbModule = module {
    single { Room.databaseBuilder(get(), AppDatabase::class.java, DATABASE_NAME).build() }
}

val allAppModules = listOf(
    navigationModule,
    dbModule,
    mainModule,
    agendaModule,
    placesModule
)