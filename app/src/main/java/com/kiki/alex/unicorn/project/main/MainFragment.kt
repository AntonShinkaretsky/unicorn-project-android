package com.kiki.alex.unicorn.project.main

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.utils.setupWithNavController
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainFragment : Fragment(R.layout.fragment_main) {

    private val vm: MainViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBottomNavigationBar()
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    // Called on first creation and when restoring state.
    private fun setupBottomNavigationBar() {

        val graphIds = listOf(
            R.navigation.graph_agenda,
            R.navigation.graph_places,
            R.navigation.graph_other
        )

        requireActivity().apply {
            // Setup the bottom navigation view with a list of navigation graphs
            val controller = bottom_navigation_main.setupWithNavController(
                navGraphIds = graphIds,
                fragmentManager = childFragmentManager,
                containerId = R.id.fragment_container_main,
                intent = intent
            )
        }
    }

//    private fun initialize() {
//        initializeBottomNavigation()
//    }
//
//    private fun observe() {
//        observeTabs()
//    }

//    private fun initializeBottomNavigation() {
//        bottom_navigation_main.setOnNavigationItemSelectedListener {
//            val pickedTab = when (it.itemId) {
//                R.id.menu_item_agenda -> AGENDA
//                R.id.menu_item_places -> PLACES
//                R.id.menu_item_other_1 -> OTHER_1
//                R.id.menu_item_other_2 -> OTHER_2
//                else -> throw IllegalStateException("picked tab can never be null")
//            }
//            vm.pickTab(pickedTab)
//
//            true
//        }
//    }
//
//    private fun observeTabs() {
//        vm.pickedTab.observe(viewLifecycleOwner, Observer { pickedTab ->
//            when (pickedTab) {
//                AGENDA -> navigateToAgenda()
//                PLACES -> navigateToPlaces()
//                OTHER_1 -> navigateToOther1()
//                OTHER_2 -> navigateToOther2()
//                else -> throw IllegalStateException("picked tab can never be null")
//            }
//        })
//    }
//
//    private fun navigateToAgenda() {
//        childFragmentManager
//            .beginTransaction()
//            .replace(R.id.frame_main, vm.agendaFragment, FlowAgendaFragment.TAG)
//            .commit()
//    }
//
//    private fun navigateToPlaces() {
//        childFragmentManager
//            .beginTransaction()
//            .replace(R.id.frame_main, vm.placesFragment, FlowPlacesFragment.TAG)
//            .commit()
//    }
//
//    private fun navigateToOther1() {
//        childFragmentManager
//            .beginTransaction()
//            .replace(R.id.frame_main, vm.otherFragment1, "OtherFragment1")
//            .commit()
//    }
//
//    private fun navigateToOther2() {
//        childFragmentManager
//            .beginTransaction()
//            .replace(R.id.frame_main, vm.otherFragment2, "OtherFragment2")
//            .commit()
//    }

}