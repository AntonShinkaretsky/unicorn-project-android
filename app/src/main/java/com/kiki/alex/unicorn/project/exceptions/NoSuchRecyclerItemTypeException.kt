package com.kiki.alex.unicorn.project.exceptions

import java.lang.RuntimeException

class NoSuchRecyclerItemTypeException : RuntimeException()