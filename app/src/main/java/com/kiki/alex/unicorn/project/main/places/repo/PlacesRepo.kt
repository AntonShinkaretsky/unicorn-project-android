package com.kiki.alex.unicorn.project.main.places.repo

import androidx.lifecycle.LiveData
import com.kiki.alex.unicorn.project.main.places.models.Place
import com.kiki.alex.unicorn.project.main.places.models.PlaceMapper.toUi
import com.kiki.alex.unicorn.project.main.places.models.PlacesApi
import com.kiki.alex.unicorn.project.main.places.models.PlacesDao
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class PlacesRepo(
    private val apiPlaces: PlacesApi,
    private val daoPlaces: PlacesDao
) {

    private var getPlaces = Disposables.disposed()

    fun getPlace(id: String): LiveData<Place> {
        return daoPlaces.get(id)
    }

    fun getPlacesLive(): LiveData<List<Place>> {
        return daoPlaces.getAllLive()
    }

    fun apiGetPlaces() {
        // TODO loading state?
        getPlaces.dispose()
        getPlaces = apiPlaces
            .getPlaces()
            .subscribeOn(Schedulers.io())
            .subscribeBy(
                onSuccess = { placesApi ->
                    val places = placesApi.map { toUi(it) }
                    daoPlaces.insertAll(places)
                    getPlaces.dispose()
                },
                onError = {
                    getPlaces.dispose()
                    // TODO
                }
            )
    }


}