package com.kiki.alex.unicorn.project.main.agenda.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface EventsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(event: List<Event>)

    @Query("SELECT * FROM event WHERE id = :id")
    fun get(id: String): LiveData<Event>

    @Query("SELECT * FROM event")
    fun getAllLive(): LiveData<List<Event>>

}