package com.kiki.alex.unicorn.project.main.places.models

object PlaceMapper {

    fun toUi(place: PlaceApi): Place {
        return Place(
            id = place.id,
            name = place.name,
            description = place.description,
            latitude = place.latitude,
            longitude = place.longitude
        )
    }

}