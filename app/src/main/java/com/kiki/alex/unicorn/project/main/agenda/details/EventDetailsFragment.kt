package com.kiki.alex.unicorn.project.main.agenda.details

import androidx.fragment.app.Fragment
import com.kiki.alex.unicorn.project.R

class EventDetailsFragment : Fragment(R.layout.fragment_event_details) {

    companion object {
        const val BUNDLE_KEY_EVENT_ID = "bundle_key_event_id"
    }

}