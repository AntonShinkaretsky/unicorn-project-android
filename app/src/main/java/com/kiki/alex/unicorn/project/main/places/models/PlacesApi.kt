package com.kiki.alex.unicorn.project.main.places.models

import io.reactivex.Single
import retrofit2.http.GET

interface PlacesApi {

    @GET("places") // TODO update endpoint
    fun getPlaces(): Single<List<PlaceApi>>

}