package com.kiki.alex.unicorn.project.main.agenda

import androidx.core.os.bundleOf
import com.kiki.alex.unicorn.project.R
import com.kiki.alex.unicorn.project.main.agenda.details.EventDetailsFragment.Companion.BUNDLE_KEY_EVENT_ID
import com.kiki.alex.unicorn.project.navigation.AppNavigator
import com.kiki.alex.unicorn.project.navigation.NavigatorAction

fun AppNavigator.navigateToEventDetails(eventId: String) {
    val action = NavigatorAction.Forward(
        R.id.fragment_container_main,
        R.id.action_agendaListFragment_to_evenDetailsFragment,
        bundleOf(BUNDLE_KEY_EVENT_ID to eventId)
    )
    navigate(action)
}