package com.kiki.alex.unicorn.project.main.places.models

data class PlaceApi(
    val id: String,
    val name: String,
    val description: String,
    val latitude: Double,
    val longitude: Double
)