package com.kiki.alex.unicorn.project.main.places

import com.kiki.alex.unicorn.project.db.AppDatabase
import com.kiki.alex.unicorn.project.main.places.list.PlacesListViewModel
import com.kiki.alex.unicorn.project.main.places.mocks.PlacesApiMocks
import com.kiki.alex.unicorn.project.main.places.models.PlacesApi
import com.kiki.alex.unicorn.project.main.places.repo.PlacesRepo
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val placesModule = module {
    //    single { get<Retrofit>().instance.create(PlacesApi::class.java) }
    single { PlacesApiMocks as PlacesApi }
    single { get<AppDatabase>().daoPlaces() }

    single {
        PlacesRepo(
            apiPlaces = get(),
            daoPlaces = get()
        )
    }

    viewModel {
        PlacesListViewModel(
            navigation = get(),
            repo = get()
        )
    }
}