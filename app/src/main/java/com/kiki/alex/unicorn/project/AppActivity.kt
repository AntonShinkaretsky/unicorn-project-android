package com.kiki.alex.unicorn.project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.kiki.alex.unicorn.project.navigation.AppNavigator
import com.kiki.alex.unicorn.project.navigation.ViewNavigator
import com.kiki.alex.unicorn.project.utils.childUsesBackPressed
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class AppActivity : AppCompatActivity(R.layout.activity_app) {

    private val appNavigator: AppNavigator by inject()
    private val viewNavigator: ViewNavigator by inject { parametersOf(this@AppActivity) }

    override fun onStart() {
        super.onStart()
        appNavigator.bindViewNavigator(viewNavigator)
    }

    override fun onStop() {
        super.onStop()
        appNavigator.unbindViewNavigator()
    }

    override fun onBackPressed() {
        if (childUsesBackPressed()) return
        else super.onBackPressed()
    }

}
